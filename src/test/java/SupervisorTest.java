import Pages.EditProductPage;
import Pages.InfoPage;
import Pages.SupervisorCreateProductForm;
import Pages.SupervisorHomePage;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

import static org.testng.Assert.assertEquals;


public class SupervisorTest extends CommonTestBasis {

    private String productName = "pineapple";
    private String productPrice = "50.00";
    private String productDescription = "sweet pineapple";
    private String updatedPrice = "1500.00";

    private String productName2 = "peach";
    private String productPrice2 = "20.00";
    private String productDescription2 = "sweet peach";

    private SupervisorHomePage supervisorHomePage;
    private String productsBannerButtonSelector = "//*[@id='menu']/li[1]/a";
    private String requestForOptionContainsOfNameDropDown = "pea";

    SupervisorTest() {
        //super("Chrome");
    }

    @BeforeClass
    public void TestLogIn() {
        try {
            openBrowser();
        } catch (MalformedURLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        logIn(SUPERVISOR_LOGIN, SUPERVISOR_PASSWORD);
        supervisorHomePage = new SupervisorHomePage(driver);
    }

    // Verifying loggin to the system under Supervisor role.
    @Test(priority = 1)
    public void TestSupervisorLogIn() {
        InfoPage myInfo = supervisorHomePage.goToInfoPage();
        String infoRole = myInfo.getRole();

        assertEquals(infoRole, SUPERVISOR_ROLE, "Displayed role is wrong.");
    }

    // Verifying the creation of new product.
    @Test(priority = 2)
    public void TestSupervisorCreateProduct() {
        SupervisorCreateProductForm supervisorCreateProductForm = supervisorHomePage.goToCreateProductForm();
        supervisorCreateProductForm.setProductName(productName);
        supervisorCreateProductForm.setProductPrice(productPrice);
        supervisorCreateProductForm.setProductDescription(productDescription);

        supervisorHomePage = supervisorCreateProductForm.returnToSupervisorHomePage();
        supervisorHomePage.supervisorHomePageTable = supervisorHomePage.goToLastPage();

        String newProductName = supervisorHomePage.supervisorHomePageTable.getProductName();
        assertEquals(newProductName, productName, "Product name was saved incorrectly.");

        String newProductPrice = supervisorHomePage.supervisorHomePageTable.getProductPrice();
        assertEquals(newProductPrice, productPrice, "Product price was saved incorrectly.");

        String newProductDescription = supervisorHomePage.supervisorHomePageTable.getProductDescription();
        assertEquals(newProductDescription, productDescription, "Product description was saved incorrectly.");
    }

    //Check products' filtration by "Name" with option "contains" work correctly
    @Test(priority = 3)
    public void TestForProductsFiltrationByName() {
        SupervisorCreateProductForm supervisorCreateProductForm = supervisorHomePage.goToCreateProductForm();
        supervisorCreateProductForm.setProductName(productName2);
        supervisorCreateProductForm.setProductPrice(productPrice2);
        supervisorCreateProductForm.setProductDescription(productDescription2);
        supervisorHomePage = supervisorCreateProductForm.returnToSupervisorHomePage();
        supervisorHomePage.supervisorHomePageTable = supervisorHomePage.goToLastPage();
        String id = supervisorHomePage.supervisorHomePageTable.getProductID();

        supervisorHomePage.chooseFilterOfProductsByNameWithOptionContains();
        supervisorHomePage.supervisorHomePageTable = supervisorHomePage
                .inputRequestValueForFiltrationByName(requestForOptionContainsOfNameDropDown);

        supervisorHomePage.supervisorHomePageTable = supervisorHomePage.goToLastPageIfAvailable(supervisorHomePage.supervisorHomePageTable);

        String productID = supervisorHomePage.supervisorHomePageTable.getProductID();

        assertEquals(productID, id, "Filtration of products works incorrectly.");
    }

    // Verifying edit function for price of existing product.
    @Test(priority = 4)
    public void TestEditProduct() {
        EditProductPage editProduct = supervisorHomePage.supervisorHomePageTable.goToEditProductPage();
        editProduct.setNewPrice(updatedPrice);
        supervisorHomePage = editProduct.returnToSupervisorHomePage();

        String newPrice = supervisorHomePage.supervisorHomePageTable.getNewPrice();
        assertEquals(newPrice, updatedPrice, "Product price was saved incorrectly.");
    }

    // Return to supervisor home page.
    @AfterMethod
    public void GoToSupervisorHomePage() {
        driver.findElement(By.xpath(productsBannerButtonSelector)).click();
        supervisorHomePage = new SupervisorHomePage(driver);
    }
}