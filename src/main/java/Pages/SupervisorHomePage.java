package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


public class SupervisorHomePage extends CommonWebPage {

    public SupervisorHomePageTable supervisorHomePageTable;
    @FindBy(xpath = "//input[@value=\"Create Product\"]")
    private WebElement createProductButton;
    @FindBy(id = "last")
    private WebElement buttonLastPage;
    @FindBy(id = "name_options")
    private WebElement dropDownName;
    @FindBy(id = "name_input")
    private WebElement textBoxForDropDownName;
    @FindBy(id = "apply_button")
    private WebElement applyButton;
    private String optionContainsOfFilterName = "contains";


    

    /**
     * Class constructor
     *
     * @param driver
     */
    public SupervisorHomePage(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(createProductButton));
        supervisorHomePageTable = new SupervisorHomePageTable(driver);
    }

    public SupervisorCreateProductForm goToCreateProductForm() {
        createProductButton.click();
        return new SupervisorCreateProductForm(webDriver);
    }

    // Go to the last page of "Products" tab at SupervisorHomePage to verify the presence of new created product.
    public SupervisorHomePageTable goToLastPage() {
        buttonLastPage.click();
        return new SupervisorHomePageTable(webDriver);
    }

    // Choose option "contains" in drop-down "Name"
    public void chooseFilterOfProductsByNameWithOptionContains() {
        dropDownName.click();
        Select selectOption = new Select(dropDownName);
        selectOption.selectByVisibleText(optionContainsOfFilterName);
    }

    // Input requested value for filtration to the text-box for drop-down "Name" and apply the filter
    public SupervisorHomePageTable inputRequestValueForFiltrationByName(String option) {
        textBoxForDropDownName.sendKeys(option);
        applyButton.click();
        return new SupervisorHomePageTable(webDriver);
    }

    public SupervisorHomePageTable goToLastPageIfAvailable(SupervisorHomePageTable table) {
        webDriver.findElement(By.id("last"));
        if(buttonLastPage.isEnabled()){
            buttonLastPage.click();
        }
        return table;
    }
}
