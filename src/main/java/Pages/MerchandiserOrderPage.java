package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;


public class MerchandiserOrderPage extends CommonWebPage {

    @FindBy (id = "save")
    private WebElement saveButton;

    @FindBy (id = "status_checkbox")
    private WebElement statusCheckboxesText;

    @FindBy (id = "products")
    private WebElement tableOrderedProducts;

    @FindBy (id = "order_status")
    private WebElement orderStatusCheckbox;

    @FindBy (xpath = "//ul[@id=\"menu\"]/li/a[@title=\"manage_orders\"]")
    private WebElement menuButtonOrder;

    @FindBy (xpath="//p[@id = \"status_checkbox\"]/input")
    private List<WebElement> checkboxStatus;

    @FindBy (id = "order_number")
    private WebElement orderId;

    /**
     * Constructor
     */
    public MerchandiserOrderPage(WebDriver driver){
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(tableOrderedProducts));
        waitForEndOfAllAjaxes();
    }

    //Returns the text of status checkbox
    public String getStatusCheckboxText(){
        String checkboxText = "";
        checkboxText = statusCheckboxesText.getText();
        return checkboxText;
    }

    //Returns the quantity of checkboxes for changing order's status
    public int getStatusCheckboxQuantity(){
        return checkboxStatus.size();
    }

    public String getOrderId(){
        return orderId.getText();
    }

    //Changes the order's status from ordered to delivered
    public MerchandiserOrderPage changeOrdersStateToNextState(){
        orderStatusCheckbox.click();
        return saveOrderChanges();
    }

    public MerchandiserOrderPage saveOrderChanges(){
        saveButton.click();

        webWait.until(ExpectedConditions.alertIsPresent());
        alertAccept();

        return new MerchandiserOrderPage(webDriver);
    }

    public MerchandiserHomePage returnToHomePage(){
        menuButtonOrder.click();
        return new MerchandiserHomePage(webDriver);
    }
}
