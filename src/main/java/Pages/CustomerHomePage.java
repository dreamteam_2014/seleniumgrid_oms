package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


public class CustomerHomePage extends CommonWebPage {

    @FindBy(xpath = "//input[@value=\"Create New Order\"]")
    private WebElement buttonCreateNewOrder;
    // descriptions on the navigation
    @FindBy(xpath = ".//span[@id='pages_amount']")
    private WebElement ammountOfOrderPage;
    @FindBy(xpath = ".//span[@id='page']")
    private WebElement numberOfPage;
    // buttons 0n the grid
    @FindBy(xpath = "//input[@id='last']")
    private WebElement buttonLast;

    @FindBy(xpath = ".//select[@id='orders_options']")
    private WebElement selectOrderBy;
    @FindBy(xpath = ".//select[@id='status_options']")
     private WebElement selectByStatus;

    @FindBy(xpath = ".//input[@id='apply_button']")
    private WebElement buttonApply;

    @FindBy(xpath = "//table/tr[last()]//img[@class='edit_img']")
    private WebElement buttonLastRowEditTable;
    @FindBy(xpath = "//table/tr[last()]//img[@class='delete_img']")
    private WebElement buttonLastOrderDelete;
    @FindBy(xpath = "//table/tr[last()]/td[1]")
    private WebElement idOfLastOrder;


    private static final String STATE_SELECTOR_BY_ORDER_NUMBER = "Order Number";
    private static final String STATE_SELECTOR_STATUS_CREATED = "Created";

    By rowOfCreatedOrders = By.xpath(".//*[@id='grid']/tr");




    public CustomerHomePage(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(buttonCreateNewOrder));
        waitForEndOfAllAjaxes();
    }

    public CustomerOrderDetail createNewOder() {
        buttonCreateNewOrder.click();
        return new CustomerOrderDetail(webDriver);
    }

    public void goToLastPageOfOrders(){
              if(Integer.parseInt(ammountOfOrderPage.getText())>1){
                  buttonLast.click();
                  waitForEndOfAllAjaxes();
              }
    }

    public CustomerOrderInfo clickToEdit() {
            goToLastPageOfOrders();
            buttonLastRowEditTable.click();
        return new CustomerOrderInfo(webDriver);
    }


    public String getLastOrderId(){
        goToLastPageOfOrders();
        if(webDriver.findElements(rowOfCreatedOrders).size()>0){
             return idOfLastOrder.getText();
        }
           else return "All Order deleted";
    }


    public CustomerHomePage clickToDeleteOrder(){
        buttonLastOrderDelete.click();
        while (checkAlert()){
            alertAccept();
        }
       return new CustomerHomePage(webDriver);
    }


    public void setFilterOrderByOptionOrdersNumber() {
        choseSelectorOption(selectOrderBy, STATE_SELECTOR_BY_ORDER_NUMBER);
    }

    public void setFilterOrderByOptionCreate() {
        choseSelectorOption(selectByStatus, STATE_SELECTOR_STATUS_CREATED);
    }
    public CustomerHomePage applyFilter(){
        buttonApply.click();
        return new CustomerHomePage(webDriver);
    }
}
