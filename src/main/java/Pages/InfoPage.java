package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Class representing info page
 */
public class InfoPage extends CommonWebPage {
    @FindBy (xpath = "//span[@id=\"user_role\"]")
    private WebElement roleLabel;

    /**
     * Class constructor
     * @param driver
     */
    public InfoPage(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(roleLabel));
        waitForEndOfAllAjaxes();
    }

    public String getRole(){
        return roleLabel.getText();
    }
}
