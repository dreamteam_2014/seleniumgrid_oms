package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class MerchandiserHomePageTable extends CommonWebPage {

    @FindBy (xpath = "//*[@id='grid']/tr[1]")
    private WebElement tableHeader;

    @FindBy (xpath = "//table[@id=\"grid\"]/tr/th")
    private List<WebElement> headTabCollection;

    private List<WebElement> rowsCollection;
    private String rowsCollectionXpath = "//table[@id=\"grid\"]/tr[@id]";


    /**
     * Constructor
     */
    public MerchandiserHomePageTable(WebDriver driver){
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(headTabCollection.get(0)));
        waitForEndOfAllAjaxes();
    }

    //Returns the list of table's titles
    public ArrayList getTableHeader(){
        ArrayList<String> listHeaders = new ArrayList<String>();
        for(int i=0; i< headTabCollection.size();i++){
            listHeaders.add(headTabCollection.get(i).getText());
        }
        return listHeaders;
    }

    //Returns the list of orders' statuses visible for merchandiser
    public HashSet getOrdersStatuses(){
        HashSet<String> existOrdersStatuses = new HashSet<String>();
        String currentStatus = "";
        rowsCollection = webDriver.findElements(By.xpath(rowsCollectionXpath));
        for(int i=0; i<rowsCollection.size();i++){
            currentStatus = rowsCollection.get(i).findElement(By.xpath(".//td[4]")).getText();
            existOrdersStatuses.add(currentStatus);
        }
        return existOrdersStatuses;
    }

    //Returns the page of the second order in the table
    public MerchandiserOrderPage getOrderPage(){
        rowsCollection = webDriver.findElements(By.xpath(rowsCollectionXpath));
        rowsCollection.get(1).findElement(By.xpath(".//td[7]/img")).click();
        return new MerchandiserOrderPage(webDriver);
    }

    //Returns the status of order found by its id
    public String getOrderStatusById(String orderId){
        String currentStatus = "";

        rowsCollection = webDriver.findElements(By.xpath(rowsCollectionXpath));
        for(int i=0; i< rowsCollection.size();i++){
            if(rowsCollection.get(i).findElement(By.xpath(".//td[1]")).getText().equals(orderId)){
                currentStatus = rowsCollection.get(i).findElement(By.xpath(".//td[4]")).getText();
                break;
            }
        }
        return currentStatus;
    }

    public MerchandiserOrderPage getOrderPageById(String orderId){
        MerchandiserOrderPage merchandiserOrderPage = null;
        rowsCollection = webDriver.findElements(By.xpath(rowsCollectionXpath));
        WebElement currentOrderId;
        for(int j=0;j<rowsCollection.size();j++){
            currentOrderId = rowsCollection.get(j).findElement(By.xpath(".//td[1]"));
            if(currentOrderId.getText().equals(orderId)){
                rowsCollection.get(j).findElement(By.xpath(".//td[7]/img")).click();
                return new MerchandiserOrderPage(webDriver);
            }
        }
        return merchandiserOrderPage;
    }


    public boolean ifOrderExistsById(String orderId) {
        rowsCollection = webDriver.findElements(By.xpath(rowsCollectionXpath));
        WebElement currentOrderId;
        for(int j=0;j<rowsCollection.size();j++){
            currentOrderId = rowsCollection.get(j).findElement(By.xpath(".//td[1]"));
            if(currentOrderId.getText().equals(orderId)){
                return true;
            }
        }
        return false;
    }
}
