package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Class representing login page
 */
public class LoginPage extends CommonWebPage {

    /**
     * Page constructor
     * @param driver
     */

    @FindBy(id="name")
    private WebElement fieldLogin;
    @FindBy(id="password")
    private WebElement fieldPassword;
    @FindBy(id="post_form")
    private WebElement loginButton;

    public LoginPage(WebDriver driver){
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(loginButton));
        waitForEndOfAllAjaxes();
    }

    public void logInProcedure(String login, String password){
        setLogin(login);
        setPassword(password);
        SubmitLogIn();
    }

    private void setLogin(String login){
        fieldLogin.sendKeys(login);
    }

    private void setPassword(String password){
       fieldPassword.sendKeys(password);
    }

    private void SubmitLogIn(){
        loginButton.submit();
        if (0 < webDriver.findElements(By.xpath("//pre")).size()) {
            webDriver.navigate().refresh();
            if(checkAlert()){
                alertAccept();
            }
        }

    }



}

